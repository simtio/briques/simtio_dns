from bottle import request, redirect
import re
import random
import string
from core import BasePlugin, MenuItem, DockerManager, Config, Wizard, Page
from core.helpers import Flash


class Plugin(BasePlugin):
    def __init__(self):
        super(Plugin, self).__init__()
        self.register_menu_item(MenuItem("DNS", 'fas fa-cubes', "/plugins/dns/"))

        self.register_page(Page('', Page.METHOD_GET, self.action_index))
        self.register_page(Page('', Page.METHOD_POST, self.action_index_process))
        self.register_page(Page('delete', Page.METHOD_POST, self.action_delete_process))

        self.register_wizard(Wizard(
            config_key="dns_domain",
            question=self.strings.wizard_domain,
            response_type=Wizard.TYPE_STRING
        ))

    def add_entry(self, name, dest, entry_type):
        current = self.config.get("dns_entries")
        if current is None:
            current = []
        new = {"name": name,
               "type": entry_type,
               "dest": dest}
        allowed = True
        if request.forms.get('type') == 'A':
            if not re.match(r"^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$", request.forms.get('dest')):
                allowed = False
                return {'success': False, 'message': self.strings.ip_required}

        if allowed:
            current_names = []
            for c in current:
                current_names.append(c['name'])
            if new['name'] not in current_names:
                current.append({"name": name,
                                "type": entry_type,
                                "dest": dest})
                self.config.set("dns_entries", current)
                params = [
                    '-e SERVER="{0}"'.format(Config().get("worker_ip")),
                    '-e DOMAIN="{0}"'.format(self.config.get("dns_domain")),
                    '-e SECRET="{0}"'.format(self.config.get("dns_key")),
                    '-e COMMAND="update add {0}.{1}. 0 {2} {3}"'.format(name,
                                                                        self.config.get("dns_domain"),
                                                                        entry_type,
                                                                        dest)
                ]
                o = DockerManager().deploy_temp('simtio/dns_nsupdate:' + self.get_version(),
                                                ' '.join(params))

                if o.returncode == 0:
                    return {'success': True, 'message': self.strings.entry_added}
                else:
                    return {'success': False, 'message': self.strings.entry_add_error}
            else:
                return {'success': False, 'message': self.strings.entry_exist}

    def action_index(self, msg_domain=None, msg_add=None, msg_remote_dns=None):
        return self.render("index", {"domain": self.config.get("dns_domain", ""),
                                     "entries": self.config.get("dns_entries"),
                                     "remote_dns": self.config.get("remote_dns"),
                                     "msg_domain": msg_domain,
                                     "msg_add": msg_add,
                                     "msg_remote_dns": msg_remote_dns})

    def action_index_process(self):
        request.body.read()
        msg_domain = None
        msg_add = None
        msg_remote_dns = None
        if "domain_name" in request.forms.keys():
            self.config.set("dns_domain", request.forms.get('domain_name'))
            self.config.set("dns_key", self.generate_key())
            self.deploy()
            msg_domain = self.strings.msg_domain
        elif "name" in request.forms.keys() and "dest" in request.forms.keys() and "type" in request.forms.keys():
            if request.forms.get('name') and request.forms.get('dest') and request.forms.get('type'):
                msg_add = self.add_entry(request.forms.get('name'), request.forms.get('dest'), request.forms.get('type'))
        elif "remote_dns" in request.forms.keys():
            self.config.set("remote_dns", request.forms.get('remote_dns'))
            self.deploy()
            msg_remote_dns = self.strings.msg_remote_dns

        return self.action_index(msg_domain, msg_add, msg_remote_dns)

    def action_delete_process(self):
        request.body.read()
        entry = request.forms.get("value")
        currents = self.config.get("dns_entries")
        current = None
        for c in currents:
            if c['name'] == entry:
                current = c

        if current is None:
            Flash.set(Flash.TYPE_ERROR, self.strings.delete_error.format(entry))
            redirect('/plugins/dns/')
            return

        params = [
            '-e SERVER="{0}"'.format(Config().get("worker_ip")),
            '-e DOMAIN="{0}"'.format(self.config.get("dns_domain")),
            '-e SECRET="{0}"'.format(self.config.get("dns_key")),
            '-e COMMAND="update delete {0}.{1}. {2}"'.format(current['name'],
                                                             self.config.get("dns_domain"),
                                                             current['type'])
        ]
        o = DockerManager().deploy_temp('simtio/dns_nsupdate:' + self.get_version(),
                                        ' '.join(params))

        if o.returncode == 0:
            currents.remove(current)
            self.config.set("dns_entries", currents)

            Flash.set(Flash.TYPE_SUCCESS, self.strings.delete_success.format(entry))
        else:
            Flash.set(Flash.TYPE_ERROR, self.strings.delete_error.format(entry))

        redirect('/plugins/dns/')

    def randomString(stringLength=10):
        """
        Generate a random string of fixed length
        :return: string
        """
        letters = string.ascii_lowercase
        return ''.join(random.choice(letters) for i in range(stringLength))

    def generate_key(self):
        """
        Generate dnssec key
        :return: key
        """
        return DockerManager().deploy_temp('simtio/dns_dnssec-keygen:' + self.get_version(),
                                           '-e DOMAIN="{0}"'.format(self.config.get("dns_domain")))\
            .stdout.decode("utf-8").rstrip("\n")

    def install(self):
        self.config.set("dns_entries", [
            {"name": "simtio", "type": 'A', "dest": Config().get("worker_ip")}
        ])
        self.config.set("remote_dns", '1.1.1.1;1.0.0.1;')

        self.config.set("dns_key", self.generate_key())
        super().install()
