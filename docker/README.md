# SIMTIO - DNS (bind9)

This project build a bind9 domain name server configured to serve and accept updates for the root domain $BIND9_DOMAIN env variable.

Use the following ENV when running :
- "BIND9_IP" : public IP of the DNS server
- "BIND9_DOMAIN" : the root domain (like : example.com)
- "BIND9_KEY" : domain key
- "BIND9_FORWARDERS" : the forwarders (like : 8.8.8.8;8.8.8.4;), default none.
- "BIND9_QUERY_CACHE_ACCEPT": set IPs to allow in allow-query-cache, default "any;".
- "BIND9_RECURSION_ACCEPT": set IPs to allow in allow-recursion, default "any;".
- "BIND9_STATIC_ENTRIES": multiline static entries for your zone.
- "BIND9_IPV6": TRUE/FALSE, enable ipv6 listening, default FALSE.

The port 53 must be exposed in TCP/UDP to answer DNS requests. The server will accept any query, but no cache request or recursion.

## Starting the container
With automatic restart (also on-boot) :
```
docker run -d --name simtio-dns -p 53:53/udp -p 53:53 --restart=unless-stopped \
  -e "BIND9_DOMAIN=intranet.local" \
  -e "BIND9_IP=192.168.1.254" \
  -e "BIND9_KEY=c2VjcmV0" \
  -e BIND9_STATIC_ENTRIES="www CNAME a.fqdn.com
blog 60 A 10.10.10.10" simtio-dns:latest
```

For testing purpose :
```
docker run --name simtio-dns --net=host \
  -e "BIND9_DOMAIN=intranet.local" \
  -e "BIND9_IP=192.168.17.3" \
  -e "BIND9_KEY=c2VjcmV0" \
  -e BIND9_STATIC_ENTRIES="debian1 A 192.168.17.3" simtio-dns:latest
```
## Configuring

To manually generate your domaine key : `dnssec-keygen -a HMAC-MD5 -b 512 -n USER ${BIND9_DOMAIN}`, else it will be generated automatically. You need bind9utils package for this.

To manually add an entry :

```
nsupdate -y secret:c2VjcmV0
server 10.10.12.23
update add myentry.example.com 60 A 10.10.14.100
```
To get all entries of a domain with axfr request: `dig -y secret:c2VjcmV0 @10.10.12.23 example.com axfr`

See `nsupdate man` for usage.

## Miscallenious
- DHCP port : TCP/UDP 53
- Remarkable files/folders :
  - /etc/bind/named.conf
  - /etc/bind/named.conf.options
  - /etc/bind/named.conf.default-zones
  - /etc/bind/named.conf.local

## Sources
- https://wiki.debian.org/fr/Bind9
- https://www.supinfo.com/articles/single/1715-dynamic-dns-avec-bind9-isc-dhcp-server
- https://github.com/digitalLumberjack/docker-bind9
- https://www.digitalocean.com/community/tutorials/how-to-configure-bind-as-a-caching-or-forwarding-dns-server-on-ubuntu-14-04
