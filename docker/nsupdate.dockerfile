###############################
## Dockerfile dns : nsupdate ##
## SIMTIO                    ##
###############################
FROM simtio/base_image:0.5-buster

ENV SERVER '' \
    DOMAIN '' \
    COMMAND '' \
    SECRET ''

RUN apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -qy dnsutils \
  && apt-get clean \
  && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* \
  ;

WORKDIR /tmp

CMD echo "server ${SERVER}" > update && echo "${COMMAND}" >> update && echo "send" >> update && echo "quit" >> update && nsupdate -y "${DOMAIN}:${SECRET}" -v update
