#####################################
## Dockerfile dns : dnssec-keygen  ##
## SIMTIO                          ##
#####################################
FROM simtio/base_image:0.5-buster

ENV DOMAIN ''

RUN apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -qy bind9 \
  && apt-get clean \
  && rm -rf /tmp/* /var/tmp/* /var/lib/apt/lists/* \
  ;

WORKDIR /etc/bind

CMD dnssec-keygen -a HMAC-MD5 -b 512 -n HOST ${DOMAIN} > /dev/null 2>&1 && (grep Key K${DOMAIN}.*.private | cut -c6-)
