#!/usr/bin/env bash

# Get out if variable is not initialized
set -e

########################################
###==================================###
### Bind9 configuration script       ###
### Author : Alban ESPIE-GUILLON     ###
### SIMTIO                           ###
###==================================###
########################################

##------------------##
## Global Variables ##
##------------------##

SUBJECT="bind9-configuration"
VERSION="1.0.0 (21/12/2018)"
USAGE="Usage: ${0} -hv"
HELP="This script is a used to configure bind9."
DATE=$(date '+%F-%Hh%Mm')

##------------------##
## Script Variables ##
##------------------##
arg=$1

## Root Check ##
if [ "$UID" -ne "0" ]
then
   echo "[$DATE] You need to be root to execute this script."
   exit 1
fi

##-------------------##
## Option processing ##
##-------------------##

if [[ -z "${BIND9_DOMAIN}" ]];then
  echo "The variable BIND9_DOMAIN must be set"
  exit 1
fi

if [[ -z "${BIND9_IP}" ]];then
    echo "The variable BIND9_IP must be set"
    exit 1
fi

while getopts ":vh" optname
  do
    case "$optname" in
      "v")
        echo "Version $VERSION"
        exit 0;
        ;;
      "h")
        echo $HELP
        echo $USAGE
        exit 0;
        ;;
      "?")
        echo "Unknown option $OPTARG"
        exit 0;
        ;;
      ":")
        echo "No argument value for option $OPTARG"
        exit 0;
        ;;
      *)
        echo "Unknown error while processing options"
        exit 0;
        ;;
    esac
  done

shift $((${OPTIND} - 1))

# -----------------------------------------------------------------
#  SCRIPT LOGIC GOES HERE
# -----------------------------------------------------------------

mkdir -p /var/run/named /etc/bind/zones
chown -R bind:bind /etc/bind/zones/

## If domain key doesn't exist then create it
if [[ ! -e /etc/bind/${BIND9_DOMAIN}.key ]]
then

  ## Go to /etc/bind
  cd /etc/bind

  if [[ -z "${BIND9_KEY}" ]];then
    echo "The variable BIND9_KEY is not set, it will be automatically generated."
    ## Generate dnssec key
    dnssec-keygen -a HMAC-MD5 -b 512 -n USER ${BIND9_DOMAIN}

    ## Get ${BIND9_DOMAIN} key
    BIND9_KEY=$(grep Key /etc/bind/K${BIND9_DOMAIN}.*.private | cut -c6-)

    ## Get rid of generated files
    rm /etc/bind/K${BIND9_DOMAIN}.*.private /etc/bind/K${BIND9_DOMAIN}.*.key
  fi

  ## Configure
  ### Creating key configuration
  cat <<EOF > /etc/bind/${BIND9_DOMAIN}
  key "${BIND9_DOMAIN}" {
  algorithm hmac-md5;
  secret "${BIND9_KEY}";
  };
EOF

  ### Creating named configuration
  cat <<EOF > /etc/bind/named.conf.local
include "/etc/bind/${BIND9_DOMAIN}";
zone "${BIND9_DOMAIN}" {
       type master;
       file "/etc/bind/zones/db.${BIND9_DOMAIN}";
       allow-update { key "${BIND9_DOMAIN}"; } ; };
EOF

  ### Creating ${BIND9_DOMAIN} configuration
  cat <<EOF > "/etc/bind/zones/db.${BIND9_DOMAIN}"
@		IN SOA	ns.${BIND9_DOMAIN}. root.${BIND9_DOMAIN}. (
        20041125   ; serial
        604800     ; refresh (1 week)
        86400      ; retry (1 day)
        2419200    ; expire (4 weeks)
        604800     ; minimum (1 week)
        )
        NS	ns.${BIND9_DOMAIN}.
ns			A	${BIND9_IP}
${BIND9_STATIC_ENTRIES}
EOF

  ### Creating named.conf.options configuration
  if [[ -z "${BIND9_FORWARDERS}" ]];then
    forwarders=""
  else
    forwarders="forwarders {${BIND9_FORWARDERS}};"
  fi

  if [[ ${BIND9_IPV6} == 'TRUE' ]];then
    ipv6="listen-on-v6 { any; }"
  else
    ipv6=""
  fi

  cat <<EOF > "/etc/bind/named.conf.options"
  options {
  directory "/var/cache/bind";
  allow-recursion {${BIND9_RECURSION_ACCEPT}};
  allow-query-cache {${BIND9_QUERY_CACHE_ACCEPT}};
  allow-query {any;};
  recursion yes;
  ${forwarders}
  dnssec-enable yes;
  dnssec-validation yes;
  auth-nxdomain no;    # conform to RFC1035
  ${ipv6}
};
EOF
fi
exit 0
