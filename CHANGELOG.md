# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.1.3] - 2020-04-17
### Fixed
- dns_nsupdate registry

## [1.1.2] - 2020-04-17
### Fixed
- msg_remote_dns not defined [#3](https://gitlab.com/simtio/briques/simtio_dns/-/issues/3)
- nsupdate container version

## [1.1.1] - 2020-04-15
### Fixed
- Forwarders string format

## [1.1.0] - 2020-04-14
### Added
- forwarder input in ui
- entry type name
### Fixed
- dns type list position
- switch to simtio/base_image:0.5-buster

## [1.0.7] - 2019-12-18
### Fixed
- /usr/local/bin/bind9-configure.sh is a directory

## [1.0.6] - 2019-12-18
### Fixed
- Permission denied on /usr/local/bin/bind9-configure.sh

## [1.0.5] - 2019-12-18
### Added
- CI
- add_entry method
### Changed
- dynamic version for sub containers
- sub containers repository
